Your Software – Everything you need
=============

As part of a web design company, it’s part of our responsibility to get to know some tools and software used for website designing. Personally, it’s a big help for me to learn some of it because it makes things easier for me and it allows me to make quality web design for the clients.

Here are some of the tools and software I am using for website design and I am strongly recommending it to all website and graphic designers too. I used Bugherd, Fontello, Foundation 3, Dreamweaver CS6, Cloud9 IDE, Sencha Touch 2, [Website Design Melbourne](http://www.gmgweb.com.au) , Adobe Edge Inspect, Brackets, Trello, TypeCast beta, Gridset, Yeoman, Emmet, Sublime Text 2, Microsoft WebMatrix 2, PhoneGap 2.0, Firefox 18 and Photon.


PhoneGap 2.0
![Screenshot of 2D](https://katzmax.files.wordpress.com/2012/09/selection_205.png)


Cloud9 IDE
![Screenshot of 2D](http://www.ymc.ch/wp-content/uploads/2012/09/cloud9-editor1.jpg)


Dreamweaver CS6
![Screenshot of 2D](https://media.phpnuke.org/000/056/882/50e_82c_580_580-dreamweaver-cs6-free-download-suite.jpg)